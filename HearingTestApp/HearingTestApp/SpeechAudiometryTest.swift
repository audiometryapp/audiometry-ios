//
//  SpeechAudiometryTest.swift
//  HearingTestApp
//
//  Created by Andreas Thorslund Andersen on 29/10/2019.
//  Copyright © 2019 Andreas Thorslund Andersen. All rights reserved.
//
import AudioKit
import Foundation
// maybe look into voice transcription: https://www.raywenderlich.com/573-speech-recognition-tutorial-for-ios
class SpeechAudiometryTest {
    var currentWord = ""
    let speechTest = SpeechOneUpTwoDown()
    let persistence = Persistence()   
    
    //Start Test and Start Audio player
    //Also feed player with spondees
    func startTest(labelDB: UILabel, btnSentence: UIButton) {
        print("Speech test started")
        speechTest.startOneUpTwoDown(spondeeList: loadSpondeeWords(), labelDB: labelDB, btnSentence: btnSentence)
    }
    
    func stopTestPrematurely() {
        speechTest.stopTestPrematurely()
    }
    
    func isTestActive() -> Bool {
        return speechTest.testActive
    }
    func getCurrentSentence() -> String {
        return speechTest.currentWord
    }
    
    func getCurrentDB() -> Double {
        return speechTest.thisDB
    }
    
    //Compare submitted word from user with word that is played
    func submitWord(input: String) {
        speechTest.userRegistered(input: input.lowercased())
    }
    
    func didNotHear() {
        speechTest.userRegistered(input: "")
    }
    
    
    
    func loadSpondeeWords() -> Array<SpondeeSound> {
        //Load JSON file with Selected spondees, randomized
        var words = Array<SpondeeSound>()
        
        for listNumber in 1...16 {
            for fileNumber in 1...10 {
                let soundURL = "/spondeelists/list\(listNumber)/\(fileNumber).wav"
                
                let wordURL = "/spondeelists/list\(listNumber)/list\(listNumber)"
                
                print("SoundURL ", soundURL)
                var wordString : String = ""
                if let wordURL = Bundle.main.path(forResource: wordURL, ofType: "json") {
                    wordString = try! String(contentsOfFile: wordURL, encoding: .utf8)
                }
                
                let wordArray = wordString.split{ $0.isNewline }
                
                let spondee = SpondeeSound(wordString: String(wordArray[fileNumber]).lowercased(), listNumber: listNumber, fileNumber: fileNumber, soundURL: soundURL)
                
                words.append(spondee)
            }
        }
        
        return words.shuffled()
    }
      
    
}
