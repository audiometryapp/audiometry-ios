//
//  Calibration.swift
//  HearingTestApp
//
//  Created by Omar Nabil Hawwash on 21/11/2019.
//  Copyright © 2019 Andreas Thorslund Andersen. All rights reserved.
//

import Foundation
import AudioKit

class Calibration {
    
    // if button for this frequency is clicked & we're playing a tune
    // stop song
    
    // use this to set the amplitude:  oscillator.frequency = Double(currentFreq)
    
    init() {
        
    }
    
    // Definition of variables
    
    var oscillator = AKOscillator()
    var panner = AKPanner()
    var volumeControl = AKBooster()
    var persistence = Persistence()
    var isPlaying = false
    var retSPLPerFreq: Array<RETSPLValueForFreq> = [RETSPLValueForFreq(frequency: 250, delta: 11),RETSPLValueForFreq(frequency: 500, delta: 5.5),RETSPLValueForFreq(frequency: 1000, delta: 4.5),RETSPLValueForFreq(frequency: 2000, delta: 2.5),RETSPLValueForFreq(frequency: 3000, delta: 9.5),RETSPLValueForFreq(frequency: 4000, delta: 17), RETSPLValueForFreq(frequency: 6000, delta: 17) ,RETSPLValueForFreq(frequency: 8000, delta: 17.5)]
    
     
    
    func startAudio() {
        
        do {
            try AudioKit.start()
            print("AudioKit started successfully. / Calibration")
        
        } catch {
            print("AudioKit did not start. / Calibration")
        }
        
    }
       
    func stopAudio() {
        
        do {
            try AudioKit.stop()
            print("AudioKit stopped successfully. / Calibration")
        
        } catch {
            print("AudioKit did not stop. / Calibration")
        }
        
    }
    func setEar(ear: String) -> Double {
        var result = 0.0
        if (ear.lowercased() == "højre") {
            result = 1.0
        }
        if (ear.lowercased() == "venstre") {
            result = -1.0
        }
        if (ear.lowercased() == "both") {
            result = 0.0
        }
        return result
    }
    
    func stopCalibration() {
        isPlaying = false
        oscillator.stop()
        print("Oscillator stopped successfully. / Calibration")
        stopAudio()
        print("AudioKit stopped successfully. / Calibration")
        isPlaying = false
        
    }
    
    func startCalibration(btnFrequency : Double, ear: String) {
            if (isPlaying && oscillator.frequency == btnFrequency) {
               stopCalibration()
            } else {
           
           // start audiokit, then the oscillator, then set dB level and frequencies
           
            stopCalibration()
            oscillator.amplitude = 1
            oscillator.frequency = Double(btnFrequency)
            oscillator.rampDuration = 0.001
            panner = AKPanner(oscillator)
            panner.pan = setEar(ear: ear)
            AudioKit.output = panner
            
            startAudio()
            print("AudioKit started. / CalibrationStart method")
            oscillator.start()
            print("Oscillator started. / CalibrationStart method")
            isPlaying = true
       }
    
    }
    
    func startCalibratedSound(frequency : Double, dB : Double?, ear: String) {
        if (isPlaying && oscillator.frequency == frequency) {
            stopCalibration()
        } else {
        
        // start audiokit, then the oscillator, then set dB level and frequencies
        
        stopCalibration()
        
        oscillator.amplitude = calibrationConversionDBToAKAmplitude(db: dB!, frequency: frequency)
        oscillator.frequency = Double(frequency)
        oscillator.rampDuration = 0.001
        panner = AKPanner(oscillator)
        panner.pan = setEar(ear: ear)
        AudioKit.output = panner
        startAudio()
        print("AudioKit started. / CalibrationStart method")
        oscillator.start()
        print("Oscillator started. / CalibrationStart method")
        isPlaying = true
    }
    }
    
    func testCalibratedSpeech(inputdB : Double?, ear: String) {
            
        var mediaPlayer = AKPlayer()
        
            if let wordFile = try? AKAudioFile(readFileName: "/spondeelists/list1/1.wav") {
                
                mediaPlayer = AKPlayer(audioFile: wordFile)
                mediaPlayer.volume = convertDBToAKAmplitude(db: inputdB!, frequency: 1000.00)
                mediaPlayer.completionHandler = { AKLog("Done") }
                panner = AKPanner(mediaPlayer)
                panner.pan = setEar(ear: ear)
                
                stopAudio()
                
                AudioKit.output = panner
                
                startAudio()
                mediaPlayer.play()

            }

        }

    func saveAllCalibrationData(input: Array<CalibrationValues>) {
        persistence.saveCalibration(result: input)
        print("saved")
        
    }
    
    func fetchCalibrationData() throws -> Array<CalibrationValues> {
        do {
            return try persistence.fetchCalibrationData()
        }
    }
    
    func fetchCalibrationMaxOutput(frequency: Double) -> CalibrationValues {
        var array: Array<CalibrationValues>
        do {
            array = try! fetchCalibrationData()
        }
        var result = CalibrationValues()

        for cd in array {
            if cd.frequency == frequency {
                result = cd
            }
        }
        print(result)
        return result
    }
    
     func fetchRETSPL() {
        do {
            try! retSPLPerFreq =  persistence.fetchRETSPLData()
            }
        }
    
     
     func spltoHLConversion (splDB: Double, frequency: Double) -> Double {
         var delta = 0.0
         var resultHL = splDB
         for v in retSPLPerFreq {
             if frequency == v.frequency {
                 resultHL = resultHL - v.delta
                 delta = v.delta
             }
         }
         print("SPL conversion: ", splDB, " -> ", resultHL, " Delta: ", delta)
         return resultHL
     }
     
     func hlToSPLConversion(hlDB: Double, frequency: Double) -> Double {
         var delta = 0.0
         var resultSPL = hlDB
         for v in retSPLPerFreq {
             if frequency == v.frequency {
                 resultSPL = resultSPL + v.delta
                 delta = v.delta
             }
         }
         print("HL conversion: ", hlDB, " -> ", resultSPL,  " Delta: ", delta)
         return resultSPL
     }
     
     func convertDBToAKAmplitude(db: Double, frequency: Double) -> Double {
        //let result = Double(HLToSPLConversion(hlDB: db, frequency: frequency))/20 //used to be 10^db/20
         
        let dbSPL = hlToSPLConversion(hlDB: db, frequency: frequency)
        let maxOutput = fetchCalibrationMaxOutput(frequency: frequency).maxDB
        
         let result = pow(10, (dbSPL-maxOutput)/20)
         print(result)
         return result
        }
    
    func calibrationConversionDBToAKAmplitude(db: Double, frequency: Double) -> Double {
            let maxOutput = fetchCalibrationMaxOutput(frequency: frequency).maxDB
            let result = pow(10, (db-maxOutput)/20)
            print(result)
            return result
        
         }
    
    }
