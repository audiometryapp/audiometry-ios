//
//  Persistence.swift
//  HearingTestApp
//

import Foundation


class Persistence {
    
    init() {
        
    }
    
    var jsonData: Data?
    let debuggingPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent("DebuggingChecker").appendingPathExtension(".json")
    let ptrPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent("PTResults").appendingPathExtension(".json")
    let srtPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent("SRTResults").appendingPathExtension(".json")
    let calibrationPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent("Calibration").appendingPathExtension(".json")
    let retSPLPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent("RetSPLPath").appendingPathExtension(".json")
    // let retSPLPath = Bundle.main.url(forResource: "RETSPLValues", withExtension: "json") //URL(fileURLWithPath: "RETSPLValues.json")
    // Final implementation
    
    func setDebug(state: Array<Bool>) {
        
        jsonData = try? JSONEncoder().encode(state)
        do {
            try jsonData?.write(to: debuggingPath)
            print("Debugging settings file saved successfully!")
        } catch {
            print("Couldn't write Settings file.")
        }
        
        
    }
    
    func savePuretoneResults(result : Array<FreqObservation>) {
        
        jsonData = try? JSONEncoder().encode(result)
        do {
            try jsonData?.write(to: ptrPath)
        } catch {
            print("Couldn't write PTResults file.")
        }
        //let jsonString = String(data: jsonData!, encoding: .utf8)
    
    }
    
    func saveSRTResults(result : Array<SRTObservation>) {
        
        jsonData = try? JSONEncoder().encode(result)
        do {
            try jsonData?.write(to: srtPath)
        } catch {
            print("Couldn't write SRTResults file.")
        }
        //let jsonString = String(data: jsonData!, encoding: .utf8)
    
    }
   
    func saveCalibration(result : Array<CalibrationValues>) {
        print("Persistence.SaveCalibration: ", result)
        
        jsonData = try? JSONEncoder().encode(result)
        do {
            try jsonData?.write(to: calibrationPath)
        } catch {
            print("Couldn't write Calibration file.")
        }
        //let jsonString = String(data: jsonData!, encoding: .utf8)
       
    }
    
    func getDebug() -> Bool {
           do {
            jsonData = try? Data(contentsOf: debuggingPath)
            }
            if !(jsonData?.isEmpty ?? true) {
            do {
               if let state = try? JSONDecoder().decode(Array<Bool>.self, from: jsonData!) {
                return state[0]
            }
                }
           
            }
           print("couldn't retrieve debugging settings")
           return false
       }
    
    func retrievePTResults() -> Array<FreqObservation> {
     
        do {
        jsonData = try? Data(contentsOf: ptrPath)
        }
        if !(jsonData?.isEmpty ?? true) {
        do {
            if let object = try? JSONDecoder().decode(Array<FreqObservation>.self, from: jsonData!) {
            return object
        }
            }
       
        }
        return [FreqObservation(ear: "nil", freq: 0, dBTreshold: 0)]
    }
    
    func retrieveSRTResults() -> Array<SRTObservation> {
         do {
            jsonData = try? Data(contentsOf: srtPath)
            }
         if !(jsonData?.isEmpty ?? true) {
            do {
                if let object = try? JSONDecoder().decode(Array<SRTObservation>.self, from: jsonData!) {
                return object
                }
                
            }
            
        }
            return [SRTObservation(ear: "nil", dBTreshold: 0)]
        }
        
    func fetchCalibrationData() throws -> Array<CalibrationValues> {
        do {
            jsonData = try? Data(contentsOf: calibrationPath)
        }
        if !(jsonData?.isEmpty ?? true) {
        do {
            if let object = try? JSONDecoder().decode(Array<CalibrationValues>.self, from: jsonData!) {
                return object
            }
        }
        }
        return [CalibrationValues(frequency: -1, maxDB: -1)]
        }


    func fetchRETSPLData() throws -> Array<RETSPLValueForFreq> {
        // var fileData: Data?
        if !(jsonData?.isEmpty ?? true) {
        do {
            jsonData = try? Data(contentsOf: retSPLPath)
        }
        do {
            if let object = try? JSONDecoder().decode(Array<RETSPLValueForFreq>.self, from: jsonData!) {
                print("RETSPL ", object)
                return object
            }
        }
        }
        return [RETSPLValueForFreq(frequency: 5000, delta: 25)]
}
}
