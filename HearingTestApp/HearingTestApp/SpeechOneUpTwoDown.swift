//
//  oneUpTwoDown.swift
//  HearingTestApp
//

import AudioKit
import Foundation

class SpeechOneUpTwoDown {
    
    var mediaPlayer: AKPlayer!
    var thisDB = 0.0
    let baseDB = 60.0
    let MaxDB = 105.0
    let MinDB = 5.0
    var testActive = false
    var currentWord = ""
    
    var testCompleted = false
    var resultList = [SRTObservation]()
    var userRegisteredCorrect = false
    var panner: AKPanner?
    var persistence = Persistence()
    var calibration = Calibration()
    var spondeeList = Array<SpondeeSound>()
    var userSubmitted = false
    let targetPercentage = [0.4, 0.6] //ca. 50 percent
    var labelDB: UILabel = UILabel()
    var btnSentence: UIButton = UIButton()
    
    func startAudio(){
        do {
            try AudioKit.start()
            print("started AudioKit")
        } catch {
            print("failed to start audiokit")
        }
    }
    
    func stopAudio() {
        do {
            try AudioKit.stop()
            print("Stopped AudioKit")
        } catch {
            print("failed to stop audiokit")
        }
    }
    
    func startOneUpTwoDown(spondeeList: Array<SpondeeSound>, labelDB: UILabel, btnSentence: UIButton) {
        self.labelDB = labelDB
        self.btnSentence = btnSentence
        panner = AKPanner(mediaPlayer)
        self.spondeeList = spondeeList
        testActive = true
        screenLeftEar()
        screenRightEar()
        persistence.saveSRTResults(result : resultList)
        print("Your results are in! Here they are: ")
        print(persistence.retrieveSRTResults())   // Results now
        testActive = false
        testCompleted = true
    }
    
    func stopTestPrematurely() {
        testActive = false
        stopAudio()
    }
    
    func screenLeftEar() {
        screenEar(ear: "Venstre", pannerValue: -1)
    }
    
    func screenRightEar() {
        screenEar(ear: "Højre", pannerValue: 1)
    }
    
    func screenEar(ear: String, pannerValue: Double) {
        let treshold = oneUpTwoDownPerDB(pannerValue: pannerValue)
        let result = SRTObservation(ear: ear, dBTreshold: treshold)
        resultList.append(result)
        
    }
    
    func userRegistered(input: String) {
        if (input == currentWord) {
            userRegisteredCorrect = true
            print("Word matched!")
        } else {
            print("Word didn't match...")
        }
        userSubmitted = true
    }

    func oneUpTwoDownPerDB(pannerValue: Double) -> Int {
        
        
        var currentDBLevel = baseDB
        var checkedDBLevels = [DBHitCounter]()
        var currentSpondee : SpondeeSound
        var words = spondeeList
        
        while (checkForSpecificHits(receivedSet: checkedDBLevels, percentage: targetPercentage) == nil && testActive) {
            if words.isNotEmpty {
                currentSpondee = words.popLast()!
            } else {
                words = spondeeList
                currentSpondee = words.popLast()!
            }
            currentWord = currentSpondee.wordString
            
            DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
                self.btnSentence.setTitle(self.currentWord, for: .normal)
            })
            
            userSubmitted = false
            userRegisteredCorrect = false
            thisDB = currentDBLevel
            
            DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
                self.labelDB.text = String(self.thisDB)
            })
            
            print("Current word and dB: ", currentWord, currentDBLevel)
            
            if let wordFile = try? AKAudioFile(readFileName: currentSpondee.soundURL) {
                
                mediaPlayer = AKPlayer(audioFile: wordFile)
                mediaPlayer.volume = calibration.convertDBToAKAmplitude(db: currentDBLevel, frequency: 1000.00)
                mediaPlayer.completionHandler = { AKLog("Done") }
                panner = AKPanner(mediaPlayer)
                panner?.pan = pannerValue
                
                stopAudio()
                
                AudioKit.output = panner
                
                startAudio()
                mediaPlayer.play()

            }
            
            
            while (!userSubmitted) {
                if !testActive {
                    break
                }
            }
            
            if (userRegisteredCorrect){ //do this if user heard the sound  //We check if the db level was heard before
                
                if (getIndexForDB(receivedSet: checkedDBLevels, DB: currentDBLevel) == -1){ // if this level isn't heard  before
                    let currentDBHit = DBHitCounter(dBLevel: Int(currentDBLevel))
                    currentDBHit.addHit()
                    currentDBHit.addCorrectHit()
                    checkedDBLevels.append(currentDBHit)
                    print("hits: ", currentDBHit.getHits(), " ; Correct Hits: ", currentDBHit.correctHits)
                } else {    //if this level was heard before
                    let index = getIndexForDB(receivedSet: checkedDBLevels, DB: currentDBLevel)
                    checkedDBLevels[index].addHit()
                    checkedDBLevels[index].addCorrectHit()
                    print("hits: ", checkedDBLevels[index].getHits(), " ; Correct Hits: ", checkedDBLevels[index].correctHits)
                }
                    
                
                if ((currentDBLevel - 10) >= MinDB) { // goes two down
                    currentDBLevel = currentDBLevel - 10
                } else {
                    currentDBLevel = MinDB
                }
                userRegisteredCorrect = false
                
            } else {//user didn't hear sound
                if ((currentDBLevel + 5) <= MaxDB) {// adds one up
                    currentDBLevel = currentDBLevel + 5
                }
                //currentDBLevel = MaxDB
                if (getIndexForDB(receivedSet: checkedDBLevels, DB: currentDBLevel) == -1){ // if this level isn't heard  before
                    let currentDBHit = DBHitCounter(dBLevel: Int(currentDBLevel))
                    currentDBHit.addHit()
                    checkedDBLevels.append(currentDBHit)
                        
                    print("hits: ", currentDBHit.getHits(), " ; Correct Hits: ", currentDBHit.correctHits)
                 } else {
                    let index = getIndexForDB(receivedSet: checkedDBLevels, DB: currentDBLevel)
                    checkedDBLevels[index].addHit()
                        
                    print("hits: ", checkedDBLevels[index].getHits(), " ; Correct Hits: ", checkedDBLevels[index].correctHits)
                 }
                
            }
        }
        stopAudio()
        testCompleted = true
        return (checkForSpecificHits(receivedSet: checkedDBLevels, percentage: targetPercentage)?.db ?? -1)
    }

    
    //checks if a hitcounter has obtained specified hitcount
    func checkForSpecificHits(receivedSet: [DBHitCounter], percentage: Array<Double>)-> DBHitCounter? {
        for hitCounter in receivedSet {
            if hitCounter.compareHitPercentage() > percentage[0] && hitCounter.compareHitPercentage() < percentage[1] && hitCounter.getHits() >= 4  {
                return hitCounter
            }
        }
        return nil
    }
    
    //returns index for hitcounter with specified DB level
    func getIndexForDB(receivedSet: [DBHitCounter], DB: Double)-> Int {
        var index = 0
        for DBHitCounter in receivedSet {
            if Double(DBHitCounter.getDB()) == DB {
                return index
            }
            index = index + 1
        }
        return -1
    }
}
