//
//  RETSPLValueForFreq.swift
//  HearingTestApp
//
//  Created by Andreas Thorslund Andersen on 05/12/2019.
//  Copyright © 2019 Andreas Thorslund Andersen. All rights reserved.
//

import Foundation

struct RETSPLValueForFreq : Codable {
    var frequency: Double
    var delta: Double
}
