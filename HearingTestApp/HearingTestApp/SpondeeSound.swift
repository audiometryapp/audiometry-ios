//
//  SpondeeSound.swift
//  HearingTestApp
//
//  Created by Andreas Thorslund Andersen on 14/11/2019.
//  Copyright © 2019 Andreas Thorslund Andersen. All rights reserved.
//

import Foundation
import AudioKit

// Everything in this Struct will be replaced upon usage, except for 'emptySound'.
struct SpondeeSound {
    var wordString : String
    var listNumber : Int
    var fileNumber : Int
    var soundURL : String
    // var list1 = "1.wav"
//    let emptySound = Bundle.main.url(forResource: "EmptySound", withExtension: "ogg")
    
}
