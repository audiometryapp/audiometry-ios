//
//  oneUpTwoDown.swift
//  HearingTestApp
//

import AudioKit
import Foundation

class OneUpTwoDown: PureToneScreening {
    
    
    let frequencies = [250, 500, 1000, 2000, 3000, 4000, 6000, 8000]
    var resultList = [FreqObservation]()
    var userRegisteredSound = false
    var timerEnabled = true
    var panner: AKPanner?
    var persistence = Persistence()
    var calibration = Calibration()
    var thisDB = 0
    var thisFreq = 0
    let baseDB = 50
    let MaxDB = 105
    let MinDB = 5
    var labelFreq: UILabel = UILabel()
    var labelDB: UILabel = UILabel()
    
    
    override func startTest(labelFreq: UILabel, labelDB: UILabel) {
        self.labelDB = labelDB
        self.labelFreq = labelFreq
        panner = AKPanner(oscillator)
        testActive = true
        screenLeftEar()
        screenRightEar()
        persistence.savePuretoneResults(result : resultList)
        print("Your results are in! Here they are: ")
        print(persistence.retrievePTResults())   // Results now
        testActive = false
        testCompleted = true
    }
    
    func screenLeftEar() {
        panner?.pan = -1
        AudioKit.output = panner
        startAudio()
        screenEar(ear: "Venstre")
        stopAudio()
    }
    
    func screenRightEar() {
        panner?.pan = 1
        AudioKit.output = panner
        startAudio()
        screenEar(ear: "Højre")
        stopAudio()
    }
    
    func screenEar(ear: String) {
        for currentFreq in frequencies {
            let treshold = oneUpTwoDownPerDB(currentFreq: currentFreq)
            let result = FreqObservation(ear: ear, freq: currentFreq, dBTreshold: treshold)
            resultList.append(result)
        }
    }
    
    func userRegistered() {
        userRegisteredSound = true
        timerEnabled = false
    }

    @objc func fireTimer() {
        timerEnabled = false
        print("timer fired")
    }
    
    
    func oneUpTwoDownPerDB(currentFreq: Int) -> Int {
        oscillator.frequency = Double(currentFreq)
        thisFreq = currentFreq
        
        DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
            self.labelFreq.text = String(self.thisFreq)
        })
        
        var currentDBLevel = baseDB
        var checkedDBLevels = [DBHitCounter]()
        startAudio()
        
        while (checkForSpecificHits(receivedSet: checkedDBLevels, hit: 3) == nil && testActive) {
            thisDB = currentDBLevel
            
            DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
                self.labelDB.text = String(self.thisDB)
            })
            
            print(currentFreq, currentDBLevel)
            oscillator.amplitude = calibration.convertDBToAKAmplitude(db: Double(currentDBLevel), frequency: Double(currentFreq))
            startOsc()
            
            timerEnabled = true
            
            let work = DispatchWorkItem(block: {self.fireTimer()})
            DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + 4.0, execute: work)
            while (timerEnabled) {
                if (userRegisteredSound) {
                    work.cancel()
                    print("cancelled timer because user registered sound", currentDBLevel)
                }
            }
            work.cancel()
            stopOsc()
            
            if (userRegisteredSound){ //do this if user heard the sound  //We check if the db level was heard before
                
                if (getIndexForDB(receivedSet: checkedDBLevels, DB: currentDBLevel) == -1){ // if this level isn't heard  before
                    let currentDBHit = DBHitCounter(dBLevel: currentDBLevel)
                    currentDBHit.addHit()
                    checkedDBLevels.append(currentDBHit)
                } else {    //if this level was heard before
                    let index = getIndexForDB(receivedSet: checkedDBLevels, DB: currentDBLevel)
                    checkedDBLevels[index].addHit()
                }
                
                if ((currentDBLevel - 10) >= MinDB) { // goes two down
                    currentDBLevel = currentDBLevel - 10
                } else {
                    currentDBLevel = MinDB
                }
                userRegisteredSound = false
                
            } else {//user didn't hear sound
                if ((currentDBLevel + 5) <= MaxDB) {// adds one up
                    currentDBLevel = currentDBLevel + 5
                } else {
                    currentDBLevel = MaxDB
                    if (getIndexForDB(receivedSet: checkedDBLevels, DB: currentDBLevel) == -1){ // if this level isn't heard  before
                        let currentDBHit = DBHitCounter(dBLevel: currentDBLevel)
                        currentDBHit.addHit()
                        checkedDBLevels.append(currentDBHit)
                    } else {
                        let index = getIndexForDB(receivedSet: checkedDBLevels, DB: currentDBLevel)
                        checkedDBLevels[index].addHit()
                    }
                }
            }
        }
        stopAudio()
        return (checkForSpecificHits(receivedSet: checkedDBLevels, hit: 3)?.db ?? -1)
    }

    
    //checks if a hitcounter has obtained specified hitcount
    func checkForSpecificHits(receivedSet: [DBHitCounter], hit: Int)-> DBHitCounter? {
        for hitCounter in receivedSet {
            if hitCounter.getHits() == hit {
                return hitCounter
            }
        }
        return nil
    }
    
    //returns index for hitcounter with specified DB level
    func getIndexForDB(receivedSet: [DBHitCounter], DB: Int)-> Int {
        var index = 0
        for DBHitCounter in receivedSet {
            if DBHitCounter.getDB() == DB {
                return index
            }
            index = index + 1
        }
        return -1
    }
}
