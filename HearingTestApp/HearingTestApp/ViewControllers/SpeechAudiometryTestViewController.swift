//
//  SpeechAudiometryTestViewController.swift
//  HearingTestApp
//
//  Created by Andreas Thorslund Andersen on 29/10/2019.
//  Copyright © 2019 Andreas Thorslund Andersen. All rights reserved.
//

import UIKit
import AudioKit

class SpeechAudiometryTestViewController: UIViewController {
    
    @IBOutlet weak var txtFldUserInput: UITextField!
    @IBOutlet weak var lblDescritpion: UILabel!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var btnCouldNotHear: UIButton!
    var persistence = Persistence()
    
    let speechTest = SpeechAudiometryTest()
    let speechAlgorithm = SpeechOneUpTwoDown()
    var testHasBeenStarted = false
    
    @IBOutlet weak var lblDebugDB: UILabel!
 
    @IBOutlet weak var btnDebugSentence: UIButton!
    var debugEnabled = false
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        speechTest.stopTestPrematurely()
    }
    @IBOutlet weak var btnSpeechTest: UIButton!
    
    //activates Audiometry test UI
    @IBAction func TestButtonClicked(_ sender: UIButton) {
        debugEnabled = persistence.getDebug()
        if (!speechTest.isTestActive()) {
            btnSpeechTest.isHidden = true
            txtFldUserInput.isHidden = false
            lblDescritpion.isHidden = false
            btnSubmit.isHidden = false
            btnCouldNotHear.isHidden = false
            
            let work = DispatchWorkItem(block: {
                self.speechTest.startTest(labelDB: self.lblDebugDB, btnSentence: self.btnDebugSentence)
            })
            DispatchQueue.global(qos: .background).asyncAfter(deadline: .now(), execute: work)
            
            lblDebugDB.isEnabled = debugEnabled
            btnDebugSentence.isEnabled = debugEnabled
            btnDebugSentence.isHidden = !debugEnabled
            lblDebugDB.isHidden = !debugEnabled
        }
    }
    
    @IBAction func submitHeardWord(_ sender: UIButton) {
        speechTest.submitWord(input: txtFldUserInput.text?.lowercased() ?? "")
        txtFldUserInput.text = ""
        
        
        if debugEnabled {
            let work = DispatchWorkItem(block: {
                self.btnDebugSentence.setTitle(self.speechTest.getCurrentSentence(), for: .normal)
                self.lblDebugDB.text = String(self.speechTest.getCurrentDB())
            })
            DispatchQueue.main.asyncAfter(deadline: .now()+0.2, execute: work)
            
        }
        
        if !speechTest.isTestActive() {
                   let seconds = 1.0
                   DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {self.testHasBeenStarted = true }
                    
            if testHasBeenStarted {
                btnCouldNotHear.isHidden = true
                txtFldUserInput.isEnabled = false
                btnSubmit.setTitle("Se resultater", for: .normal)
                performSegue(withIdentifier: "SpeechToResults", sender: self)
        }
        }
    }
    
    @IBAction func didNotHearWord(_ sender: UIButton) {
        speechTest.didNotHear()
        txtFldUserInput.text = ""
        
        if !speechTest.isTestActive() {
            btnCouldNotHear.isHidden = true
            txtFldUserInput.isEnabled = false
            btnSubmit.setTitle("Se resultater", for: .normal)
    }
        
        if debugEnabled {
            let work = DispatchWorkItem(block: {
                self.btnDebugSentence.setTitle(self.speechTest.getCurrentSentence(), for: .normal)
                self.lblDebugDB.text = String(self.speechTest.getCurrentDB())
            })
            DispatchQueue.main.asyncAfter(deadline: .now()+0.2, execute: work)
            
        }
    }
    
    @IBAction func debugInsertSentence(_ sender: UIButton) {
        txtFldUserInput.text = speechTest.getCurrentSentence()
    }
    

}
