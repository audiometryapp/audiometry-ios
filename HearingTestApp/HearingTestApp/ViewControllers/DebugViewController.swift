//
//  DebugViewController.swift
//  HearingTestApp
//
//  Created by Andreas Thorslund Andersen on 19/12/2019.
//  Copyright © 2019 Andreas Thorslund Andersen. All rights reserved.
//

import UIKit

class DebugViewController: UIViewController {

    @IBOutlet weak var SwitchEnableDebug: UISwitch!
    var persistence = Persistence()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SwitchEnableDebug.isOn = persistence.getDebug()
        // Do any additional setup after loading the view.
    }
    @IBAction func changedState(_ sender: UISwitch) {
        let state = [SwitchEnableDebug.isOn]
        persistence.setDebug(state: state)
    }
    
}
