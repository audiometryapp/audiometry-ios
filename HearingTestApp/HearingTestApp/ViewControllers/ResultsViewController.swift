//
//  GuideViewController.swift
//  HearingTestApp
//

import UIKit
import AudioKit

class ResultsViewController: UIViewController {
    
    // TODO: Show results (viewDidAppear) for Persistence thru OUTD.
    let persistence = Persistence()
    @IBOutlet weak var resultsScrollView: UIScrollView!
    @IBOutlet weak var prtEarResultsInView: UILabel!
    @IBOutlet weak var prtFreqResultsInView: UILabel!
    @IBOutlet weak var prtdBResultsInView: UILabel!
    @IBOutlet weak var srtEarResultsInView: UILabel!
    @IBOutlet weak var srtdBResultsInView: UILabel!
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidLoad()
        print("Welcome to Results!")
        
        do {
            try? print(persistence.fetchCalibrationData())
            
            print(sortPRTbyEar(input: persistence.retrievePTResults(), ear: "Right"))
          
            sortSRT(input: persistence.retrieveSRTResults())
    }
}
    
    func sortPRTbyEar(input: Array<FreqObservation>, ear: String) {
        var earString : String = ""
        var freqString : String = ""
        var dBString : String = ""
        
        for e in input {
            earString += "\(e.ear)\n"
            freqString += "\(e.freq)\n"
            dBString += "\(e.dBTreshold)\n"
                
            prtEarResultsInView.text = earString
            prtFreqResultsInView.text = freqString
            prtdBResultsInView.text = dBString
            
        }
    }
    
    func sortSRT(input: Array<SRTObservation>) {
        var srtEarString : String = ""
        var srtdBString : String = ""
        
        for e in input {
            srtEarString += "\(e.ear)\n"
            srtdBString += "\(e.dBTreshold)\n"
            
            srtEarResultsInView.text = srtEarString
            srtdBResultsInView.text = srtdBString
        }
        
    }
        
}
