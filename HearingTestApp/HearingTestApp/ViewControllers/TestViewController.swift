//
//  TestViewController.swift
//  HearingTestApp
//

import UIKit

class TestViewController: UIViewController {
    
    var PureToneCompleted = false
    @IBOutlet weak var freqLabel: UILabel!
    
    var soundUnderNoiseTreshold = false
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidLoad()
        //print(Persistence().retrievePTResults())
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(false)
        screening.stopTest()
    }
    
    @IBAction func CheckEnvironmentButton() {
        checkEnvironment()
    }
    func checkEnvironment() {
        let envCheck = EnvironmentCheck()
        if (envCheck.check()) {
            soundUnderNoiseTreshold = true
            self.EnvironmentCheckLabel.text = "Clear to perform test"
            StartAfterEnviroCheckButton.isEnabled = true
        } else {
            soundUnderNoiseTreshold = false
            self.EnvironmentCheckLabel.text = "Too loud, find a more quiet place!"
            StartAfterEnviroCheckButton.isEnabled = false
        }
    }
    
    @IBOutlet weak var StartAfterEnviroCheckButton: UIButton!
    @IBOutlet weak var EnvironmentCheckLabel: UILabel!
    @IBOutlet weak var volumeLabel: UILabel!
    var screening = OneUpTwoDown()
    
    @IBAction func ScreeningButton(_ sender: UIButton) {
        if (!screening.testActive && !screening.testCompleted) {
            DispatchQueue.global(qos: .background).async { self.screening.startTest() }
        }
        if(screening.isActive()) {
            sender.setTitle("Det hørte jeg", for: .normal)
            screening.userRegistered()
            
        } else {
            if (!screening.testCompleted) {
                sender.setTitle("Det hørte jeg", for: .normal)
            } else {
                
                if (PureToneCompleted) {
                    performSegue(withIdentifier: "PureToneToResults", sender: self)
                } else {
                    let seconds = 1.0
                    DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {self.PureToneCompleted = true }
                    sender.setTitle("Se resultater", for: .normal)
                }
            }
            
        }
        
    }
    
    func startOsc(){
        screening.startOsc()
    }
    func stopOsc() {
        screening.stopOsc()
    }
    
    func startAudio() {
        screening.startAudio()
    }
    
    func stopAudio() {
       screening.stopAudio()
    }
    
    
    
}
