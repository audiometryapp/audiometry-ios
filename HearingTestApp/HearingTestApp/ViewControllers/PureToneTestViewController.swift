//
//  TestViewController.swift
//  HearingTestApp
//

import UIKit

class PureToneTestViewController: UIViewController {
    
    var PureToneCompleted = false
        
  
    @IBOutlet weak var lblDebugFreq: UILabel!
    @IBOutlet weak var lblDebugDB: UILabel!
    
    var debugEnabled = false
    var persistence = Persistence()
    
    var soundUnderNoiseTreshold = false
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidLoad()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(false)
        screening.stopTest()
    }
    
    @IBAction func CheckEnvironmentButton() {
        checkEnvironment()
    }
    func checkEnvironment() {
        let envCheck = EnvironmentCheck()
        if (envCheck.check()) {
            soundUnderNoiseTreshold = true
            self.EnvironmentCheckLabel.text = "Klar til test"
            StartAfterEnviroCheckButton.isEnabled = true
        } else {
            soundUnderNoiseTreshold = false
            self.EnvironmentCheckLabel.text = "For højt! Find et andet sted der er mere stille!"
            StartAfterEnviroCheckButton.isEnabled = false
        }
    }
    
    @IBOutlet weak var StartAfterEnviroCheckButton: UIButton!
    @IBOutlet weak var EnvironmentCheckLabel: UILabel!
    var screening = OneUpTwoDown()
    
    @IBAction func ScreeningButton(_ sender: UIButton) {
        if (!screening.testActive && !screening.testCompleted) {
            debugEnabled = persistence.getDebug()
            lblDebugDB?.isEnabled = debugEnabled
            lblDebugFreq?.isEnabled = debugEnabled
            lblDebugFreq.isHidden = !debugEnabled
            lblDebugDB.isHidden = !debugEnabled
            DispatchQueue.global(qos: .background).async { self.screening.startTest(labelFreq: self.lblDebugFreq, labelDB: self.lblDebugDB) }
        }
        if(screening.isActive()) {
            sender.setTitle("Det hørte jeg", for: .normal)
            screening.userRegistered()
            
            if debugEnabled {
                let work = DispatchWorkItem(block: {
                    self.lblDebugFreq.text = String(self.screening.thisFreq)
                    self.lblDebugDB.text = String(self.screening.thisDB)
                })
                DispatchQueue.main.asyncAfter(deadline: .now()+0.2, execute: work)
            }
            
        } else {
            if (!screening.testCompleted) {
                sender.setTitle("Det hørte jeg", for: .normal)
                if debugEnabled {
                    let work = DispatchWorkItem(block: {
                        self.lblDebugFreq.text = String(self.screening.thisFreq)
                        self.lblDebugDB.text = String(self.screening.thisDB)
                    })
                    DispatchQueue.main.asyncAfter(deadline: .now()+0.2, execute: work)
                }
            } else {
                
                if (PureToneCompleted) {
                    performSegue(withIdentifier: "PureToneToResults", sender: self)
                } else {
                    let seconds = 1.0
                    DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {self.PureToneCompleted = true }
                    sender.setTitle("Se resultater", for: .normal)
                }
            }
            
        }
        
    }
    
    
    func startOsc(){
        screening.startOsc()
    }
    func stopOsc() {
        screening.stopOsc()
    }
    
    func startAudio() {
        screening.startAudio()
    }
    
    func stopAudio() {
       screening.stopAudio()
    }
    
    
    
}
