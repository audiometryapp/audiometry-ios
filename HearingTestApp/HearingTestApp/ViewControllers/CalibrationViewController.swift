//
//  CalibrationViewController.swift
//  HearingTestApp
//
//  Created by Omar Nabil Hawwash on 21/11/2019.
//  Copyright © 2019 Omar Nabil Hawwash. All rights reserved.
//

import UIKit

class CalibrationViewController: UIViewController {
    
    // Object instantiation
    
   
    
    // Storyboard variables. Calibration buttons, text fields & insertion button.
    
    @IBOutlet weak var btn250: UIButton!
    @IBOutlet weak var btn500: UIButton!
    @IBOutlet weak var btn1000: UIButton!
    @IBOutlet weak var btn2000: UIButton!
    @IBOutlet weak var btn3000: UIButton!
    @IBOutlet weak var btn4000: UIButton!
    @IBOutlet weak var btn6000: UIButton!
    @IBOutlet weak var btn8000: UIButton!
    
    // Test buttons for calibration value
    @IBOutlet weak var btnPlay250: UIButton!
    @IBOutlet weak var btnPlay500: UIButton!
    @IBOutlet weak var btnPlay1000: UIButton!
    @IBOutlet weak var btnPlay2000: UIButton!
    @IBOutlet weak var btnPlay3000: UIButton!
    @IBOutlet weak var btnPlay4000: UIButton!
    @IBOutlet weak var btnPlay6000: UIButton!
    @IBOutlet weak var btnPlay8000: UIButton!
    
    @IBOutlet weak var fieldDBPlay: UITextField!
    
    @IBOutlet weak var switchEar: UISwitch!
    // Text fields
    
    @IBOutlet weak var field250: UITextField!
    @IBOutlet weak var field500: UITextField!
    @IBOutlet weak var field1000: UITextField!
    @IBOutlet weak var field2000: UITextField!
    @IBOutlet weak var field3000: UITextField!
    @IBOutlet weak var field4000: UITextField!
    @IBOutlet weak var field6000: UITextField!
    @IBOutlet weak var field8000: UITextField!
    
    @IBOutlet weak var btnSubmitValues: UIButton!
    @IBOutlet weak var btnTestSpeechCalibration: UIButton!
    
    // Global variables
    let speechtest = SpeechOneUpTwoDown()
    var timerEnabled = true
    var calibration = Calibration()
    var dBValue250: Double? = -1.0
    var dBValue500: Double? = -1.0
    var dBValue1000: Double? = -1.0
    var dBValue2000: Double? = -1.0
    var dBValue3000: Double? = -1.0
    var dBValue4000: Double? = -1.0
    var dBValue6000: Double? = -1.0
    var dBValue8000: Double? = -1.0

    
    var valuesArray: [Double : Double?] = [:]
    var valueObjectsArray = Array<CalibrationValues>()
    //var valueObjectsArray: Array<CalibrationValues>
    // Functions
    
    override func viewDidLoad() {
        valuesArray = makeKeyValueArray(input: try! calibration.fetchCalibrationData())
        let placeholder250 = String(calibration.fetchCalibrationMaxOutput(frequency: 250).maxDB)
        let placeholder500 = String(calibration.fetchCalibrationMaxOutput(frequency: 500).maxDB)
        let placeholder1000 = String(calibration.fetchCalibrationMaxOutput(frequency: 1000).maxDB)
        let placeholder2000 = String(calibration.fetchCalibrationMaxOutput(frequency: 2000).maxDB)
        let placeholder3000 = String(calibration.fetchCalibrationMaxOutput(frequency: 3000).maxDB)
        let placeholder4000 = String(calibration.fetchCalibrationMaxOutput(frequency: 4000).maxDB)
        let placeholder6000 = String(calibration.fetchCalibrationMaxOutput(frequency: 6000).maxDB)
        let placeholder8000 = String(calibration.fetchCalibrationMaxOutput(frequency: 8000).maxDB)
        field250.attributedPlaceholder = NSAttributedString(string: placeholder250)
        field500.attributedPlaceholder = NSAttributedString(string: placeholder500)
        field1000.attributedPlaceholder = NSAttributedString(string: placeholder1000)
        field2000.attributedPlaceholder = NSAttributedString(string: placeholder2000)
        field3000.attributedPlaceholder = NSAttributedString(string: placeholder3000)
        field4000.attributedPlaceholder = NSAttributedString(string: placeholder4000)
        field6000.attributedPlaceholder = NSAttributedString(string: placeholder6000)
        field8000.attributedPlaceholder = NSAttributedString(string: placeholder8000)
        
        super.viewDidLoad()
    }
    
        
    @IBAction func btn250Clicked(_ sender: Any) {
        if (calibration.isPlaying == true && calibration.oscillator.frequency == 250) {
             calibration.stopCalibration()
         } else {
             calibration.startCalibration(btnFrequency: 250, ear: chooseEar())
         }
        }
       
    @IBAction func btn500Clicked(_ sender: Any) {
        if (calibration.isPlaying == true && calibration.oscillator.frequency == 500) {
             calibration.stopCalibration()
         } else {
             calibration.startCalibration(btnFrequency: 500, ear: chooseEar())
         }
        }
       
    @IBAction func btn1000Clicked(_ sender: Any) {
        if (calibration.isPlaying == true && calibration.oscillator.frequency == 1000) {
             calibration.stopCalibration()
         } else {
             calibration.startCalibration(btnFrequency: 1000, ear: chooseEar())
         }
        }
       
    @IBAction func btn2000Clicked(_ sender: Any) {
        if (calibration.isPlaying == true && calibration.oscillator.frequency == 2000) {
             calibration.stopCalibration()
         } else {
             calibration.startCalibration(btnFrequency: 2000, ear: chooseEar())
         }
        }
       
    @IBAction func btn3000Clicked(_ sender: Any) {
        if (calibration.isPlaying == true && calibration.oscillator.frequency == 3000) {
             calibration.stopCalibration()
         } else {
             calibration.startCalibration(btnFrequency: 3000, ear: chooseEar())
         }
       }
       
    @IBAction func btn4000Clicked(_ sender: Any) {
        if (calibration.isPlaying == true && calibration.oscillator.frequency == 4000) {
             calibration.stopCalibration()
         } else {
             calibration.startCalibration(btnFrequency: 4000, ear: chooseEar())
         }
        }
       
    @IBAction func btn6000Clicked(_ sender: UIButton) {
        if (calibration.isPlaying == true && calibration.oscillator.frequency == 6000) {
                    calibration.stopCalibration()
                } else {
                    calibration.startCalibration(btnFrequency: 6000, ear: chooseEar())
                }
    }
    
    @IBAction func btn8000Clicked(_ sender: Any) {
        if (calibration.isPlaying == true && calibration.oscillator.frequency == 8000) {
            calibration.stopCalibration()
        } else {
            calibration.startCalibration(btnFrequency: 8000, ear: chooseEar())
        }
       }
    
    // convert textfield content to Double!
    func chooseEar() -> String {
        if (switchEar.isOn) {
            return "højre"
        } else {
            return "venstre"
        }
    }
        
    
    // insert button clicked
    @IBAction func submitValues(_ sender: Any) {
        // firstValue = Double?(firstField.text?)
        
        dBValue250 = Double(field250.text!)
        dBValue500 = Double(field500.text!)
        dBValue1000 = Double(field1000.text!)
        dBValue2000 = Double(field2000.text!)
        dBValue3000 = Double(field3000.text!)
        dBValue4000 = Double(field4000.text!)
        dBValue6000 = Double(field6000.text!)
        dBValue8000 = Double(field8000.text!)
        
        
        if (dBValue250 != nil) {
            valuesArray.updateValue(dBValue250, forKey: 250)
            print("KEYSPRINT", valuesArray.keys)
            //([1 : firstValue])
        } else {
            print("Field 250 is empty.")
            for (key, value) in Array(valuesArray) {
                print("We're in boyz")
                if key == 250 {
                    valuesArray.updateValue(value, forKey: 250)
                    print("Value updated")
                } else {
                    print("NOT FOUND...")
                }
        }
        }
        
        if (dBValue500 != nil) {
            valuesArray.updateValue(dBValue500, forKey: 500)
        } else {
            print("Field 500 is empty.")
            for (key, value) in Array(valuesArray) {
                print("We're in boyz")
                if key == 500 {
                    valuesArray.updateValue(value, forKey: key)
                    print("Value updated")
                } else {
                    print("NOT FOUND...")
                }
        }
        }
        
        if (dBValue1000 != nil) {
            valuesArray.updateValue(dBValue1000, forKey: 1000)
           } else {
                         print("Field 1000 is empty.")
                         for (key, value) in Array(valuesArray) {
                             if key == 1000 {
                                 valuesArray.updateValue(value, forKey: key)
                                 print("Value updated")
                     }
                     }
                     }
        
        if (dBValue2000 != nil) {
            valuesArray.updateValue(dBValue2000, forKey: 2000)
                  } else {
                          print("Field 2000 is empty.")
                          for (key, value) in Array(valuesArray) {
                              if key == 2000 {
                                  valuesArray.updateValue(value, forKey: key)
                                  print("Value updated")
                      }
                      }
                      }
        
        if (dBValue3000 != nil) {
            valuesArray.updateValue(dBValue3000, forKey: 3000)
            } else {
                          print("Field 3000 is empty.")
                          for (key, value) in Array(valuesArray) {
                              if key == 3000 {
                                  valuesArray.updateValue(value, forKey: key)
                                  print("Value updated")
                      }
                      }
                      }
        
        if (dBValue4000 != nil) {
            valuesArray.updateValue(dBValue4000, forKey: 4000)
                   } else {
                          print("Field 4000 is empty.")
                          for (key, value) in Array(valuesArray) {
                              if key == 4000 {
                                  valuesArray.updateValue(value, forKey: key)
                                  print("Value updated")
                      }
                      }
                      }
        
        if (dBValue6000 != nil) {
            valuesArray.updateValue(dBValue6000, forKey: 6000)
                   } else {
                          print("Field 6000 is empty.")
                          for (key, value) in Array(valuesArray) {
                              if key == 6000 {
                                  valuesArray.updateValue(value, forKey: key)
                                  print("Value updated")
                      }
                      }
                      }
        
        if (dBValue8000 != nil) {
            valuesArray.updateValue(dBValue8000, forKey: 8000)
            } else {
                          print("Field 8000 is empty.")
                          for (key, value) in Array(valuesArray) {
                              if key == 8000 {
                                  valuesArray.updateValue(value, forKey: key)
                                  print("Value updated")
                      }
                      }
                      }
        
        valueObjectsArray = makeObjectArray(input: valuesArray)
        
        print("valueObjectsArray: ", valueObjectsArray)
        
        calibration.saveAllCalibrationData(input: valueObjectsArray)
        
        
    }
    
    
    func makeObjectArray(input: [Double : Double?]) -> Array<CalibrationValues> {
        var result = Array<CalibrationValues>()
        for (key, value) in Array(input) {
            let calibrationValue = CalibrationValues(frequency: key, maxDB: value!)
            result.append(calibrationValue)
        }
        return result
    }
    
    func makeKeyValueArray(input: Array<CalibrationValues>) -> [Double : Double?] {
        var result: [Double : Double?] = [:]
        
        for v in input {
            result.updateValue(v.maxDB, forKey: v.frequency)
            
        }
        return result
    }
    
    @IBAction func play250Btn(_ sender: Any) {
        if (calibration.isPlaying == true && calibration.oscillator.frequency == 250) {
            calibration.stopCalibration()
        } else {
            var dB : Double?
            dB = Double(fieldDBPlay.text!)
            calibration.startCalibratedSound(frequency: 250, dB: dB, ear: chooseEar())
        }
    }
    
    @IBAction func play500Btn(_ sender: Any) {
        if (calibration.isPlaying == true && calibration.oscillator.frequency == 500) {
            calibration.stopCalibration()
        } else {
            var dB : Double?
            dB = Double(fieldDBPlay.text!)
            calibration.startCalibratedSound(frequency: 500, dB: dB, ear: chooseEar())
        }
    }
    
    @IBAction func play1000Btn(_ sender: Any) {
        if (calibration.isPlaying == true && calibration.oscillator.frequency == 1000) {
            calibration.stopCalibration()
        } else {
            var dB : Double?
            dB = Double(fieldDBPlay.text!)
            calibration.startCalibratedSound(frequency: 1000, dB: dB, ear: chooseEar())
        }
    }
    
    @IBAction func play2000Btn(_ sender: Any) {
        if (calibration.isPlaying == true && calibration.oscillator.frequency == 2000) {
            calibration.stopCalibration()
        } else {
            var dB : Double?
            dB = Double(fieldDBPlay.text!)
            calibration.startCalibratedSound(frequency: 2000, dB: dB, ear: chooseEar())
        }
    }
    
    @IBAction func play3000Btn(_ sender: Any) {
        if (calibration.isPlaying == true && calibration.oscillator.frequency == 3000) {
            calibration.stopCalibration()
        } else {
            var dB : Double?
            dB = Double(fieldDBPlay.text!)
            calibration.startCalibratedSound(frequency: 3000, dB: dB, ear: chooseEar())
        }
    }
    
    @IBAction func play4000Btn(_ sender: Any) {
        if (calibration.isPlaying == true && calibration.oscillator.frequency == 4000) {
            calibration.stopCalibration()
        } else {
            var dB : Double?
            dB = Double(fieldDBPlay.text!)
            calibration.startCalibratedSound(frequency: 4000, dB: dB, ear: chooseEar())
        }
    }
    
    @IBAction func play6000Btn(_ sender: Any) {
        if (calibration.isPlaying == true && calibration.oscillator.frequency == 6000) {
            calibration.stopCalibration()
        } else {
            var dB : Double?
            dB = Double(fieldDBPlay.text!)
            calibration.startCalibratedSound(frequency: 6000, dB: dB, ear: chooseEar())
        }
    }
    
    @IBAction func play8000Btn(_ sender: Any) {
        if (calibration.isPlaying == true && calibration.oscillator.frequency == 8000) {
            calibration.stopCalibration()
        } else {
            var dB : Double?
            dB = Double(fieldDBPlay.text!)
            calibration.startCalibratedSound(frequency: 8000, dB: dB, ear: chooseEar())
        }
    }
    
    
    @IBAction func testSpeechCalibration(_ sender: Any) {
        calibration.testCalibratedSpeech(inputdB: Double(fieldDBPlay.text!), ear: chooseEar())
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
         print("Welcome to the Calibration page!")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        calibration.stopCalibration()
    }

}
