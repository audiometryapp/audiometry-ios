//
//  FreqObservation.swift
//  HearingTestApp
//

import Foundation

struct FreqObservation : Codable {
    var ear = "left"
    var freq = 250
    var dBTreshold = 0
}
