//
//  PureToneScreening.swift
//  HearingTestApp
//

import Foundation
import AudioKit


class PureToneScreening {
    var oscillator = AKOscillator()
    var maxFreq: Int {return 250}
    var maxAmp: Double {return 0.5}
    var testActive = false
    var testCompleted = false
    
    init() {
        oscillator.frequency = 250
        oscillator.amplitude = 1
        oscillator.rampDuration = 0.001
    }
    
    func startAudio(){
        do {
            try AudioKit.start()
            print("started AudioKit")
        } catch {
            print("failed to start audiokit")
        }
    }
    
    func stopAudio(){
        do {
            try AudioKit.stop()
            print("Stopped AudioKit")
        } catch {
            print("failed to stop audiokit")
        }
    }
    func stopTest() {
        testActive = false
        stopOsc()
        stopAudio()
    }
    
    func startOsc() {
        oscillator.start()
    }
    func stopOsc() {
        oscillator.stop()
    }
    
    func oscillatorEnabled() -> Bool {
        return oscillator.isStarted
    }
    func isActive() -> Bool{
        return testActive
    }
    
    func startTest(labelFreq: UILabel, labelDB: UILabel) {
        // will be inherited and overwritten in OneUpTwoDown
    }
}
