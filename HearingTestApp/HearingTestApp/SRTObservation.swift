//
//  SRTObservation.swift
//  HearingTestApp
//
//  Created by Andreas Thorslund Andersen on 14/11/2019.
//  Copyright © 2019 Andreas Thorslund Andersen. All rights reserved.
//

import Foundation

struct SRTObservation : Codable {
    var ear = "left"
    var dBTreshold = 0
}
