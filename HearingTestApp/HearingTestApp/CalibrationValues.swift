//  CalibrationValues.swift
//  HearingTestApp
//
//  Created by Omar Nabil Hawwash on 04/12/2019.
//  Copyright © 2019 Andreas Thorslund Andersen. All rights reserved.
//

import Foundation

// Struct used for calibration values in SPL.

struct CalibrationValues : Codable {
    var frequency = 0.0
    var maxDB = 0.0
}
